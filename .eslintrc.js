module.exports = {
  extends: [
    'airbnb-base',
  ],
  env: {
    node: true,
  },
  rules: {
    'no-console': 'off',
    semi: ['error', 'never'],
    'no-unexpected-multiline': 'error',
    'no-nested-ternary': 'off',
    'space-unary-ops': ['error', {
      words: true,
      nonwords: false,
      overrides: {
        '!': true,
      },
    }],
    'no-unused-vars': ['error', {
      argsIgnorePattern: '^_$',
      vars: 'all',
      args: 'after-used',
      ignoreRestSiblings: true,
    }],
    'arrow-parens': ['error', 'as-needed'],
  }
}
