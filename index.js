#!/usr/bin/env node
const { spawn } = require('child_process')
const { Observable } = require('rxjs')
const {
  curry,
  head,
  identity,
  join,
  map,
  memoize,
  pipe,
  prop,
  propEq,
  replace,
  split,
  toString,
  trim,
  zipObj,
} = require('ramda')
const { sprintf } = require('sprintf-js')

const id = identity
const $ = pipe

const fg = curry((color, string) =>
  sprintf('^fg(%s)%s^fg()', color, string))

const bg = curry((color, string) =>
  sprintf('^bg(%s)%s^bg()', color, string))

const cmd = c =>
  Observable.create(obs => {
    const p = spawn(c[0], c.slice(1))
    p.stdout.on('data', x => obs.next(x))
    p.stdout.on('close', () => obs.unsubscribe())
  })
    .map($(toString, trim, split('\n')))

const memoCmd = memoize(cmd)
const stripQuotes = replace(/"(\w+)"/, '$1')

const xprop = cmd(['xprop', '-root', '-spy'])
  .flatMap(id)
  .map($(split(/\s*[:=]\s*/), zipObj(['key', 'value'])))

const desktopNames$ = xprop
  .filter(propEq('key', '_NET_DESKTOP_NAMES(UTF8_STRING)'))
  .map($(prop('value'), split(', '), map(stripQuotes)))

const activeDesktop$ = xprop
  .filter(propEq('key', '_NET_CURRENT_DESKTOP(CARDINAL)'))
  .map($(prop('value'), Number))

const activeWindow$ = xprop
  .filter(propEq('key', '_NET_ACTIVE_WINDOW(WINDOW)'))
  .map($(prop('value'), split('# '), head))
  .flatMap(winId =>
    memoCmd(['xprop', '-id', winId])
      .flatMap(id)
      .map($(split(/\s*[:=]\s*/), zipObj(['key', 'title']))
      .filter(propEq('key', '_NET_WM_NAME(UTF8_STRING)'))
      .map($(prop('title'), stripQuotes))))

const workspace$ = Observable
  .combineLatest([activeDesktop$, desktopNames$])
  .map(([active, names]) => names.map((e, i) => (
    active === i
      ? bg('#369', ` ${e} `)
      : fg('#666', ` ${e} `)
    )))
  .map(join(''))

const widgets = [
  workspace$,
  activeWindow$,
]

const dzen = spawn('dzen2', ['-ta', 'l'])
Observable.combineLatest(widgets)
  .distinctUntilChanged()
  .map(join(''))
  .do(str => dzen.stdin.write(`${str}\n`))
  .subscribe(console.log, console.log, process.exit)
